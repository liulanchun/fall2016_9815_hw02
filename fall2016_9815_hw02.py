# -*- coding: utf-8 -*-
"""
Created on Wed Sep 21 10:44:32 2016

@author: liu
"""

import argparse
import csv
from hw2Cls import *



try:
    # get commandline args
    parser = argparse.ArgumentParser(version='1.0')
    parser.add_argument('-y', action="store", dest="y")
    parser.add_argument('-d', action="store", dest="d")
    args = parser.parse_args()
    y = args.y
    d = args.d
    
    #y = "yc.csv"
    #d = "mortgages.csv"
    #print y,d
    
    #read YC
    with open(y, 'rb') as f:
        reader = csv.reader(f)
        yclist = list(reader)
    
    yclist.pop(0) #remove header row
    YC = {}
    for itm in yclist:
        YC[float(itm[0])] = float(itm[1])
    
    #create the Yield curve object
    yc = YieldCurve(YC)
    #rates = yc.getRates()
    #df = yc.discountFactor(360)
    
    
    """ read mortgages """
    mtgpkg = MtgPkg()
    with open(d, 'rb') as f:
        reader = csv.reader(f)
        yclist = list(reader)
    
    yclist.pop(0) #remove header row
    """Add mortgages to the package"""
    for itm in yclist:
        mtgpkg.addMtg(float(itm[0]),float(itm[1])/100.,int(itm[2])*12)
    
    """calculate"""
    print "The total value of the mortgage package is " ,  mtgpkg.getTtlValue(yc)
    print "The McCauley duration is ", mtgpkg.getMcCauleyDuration(yc)
    print "The McCauley convexity is ", mtgpkg.getMcCauleyConvexity(yc)
    print "The modified duration is ", mtgpkg.getModifiedDuration(yc)
    print "The modified convexity is ", mtgpkg.getModifiedConvexity(yc)
    print "DV01 is ", mtgpkg.getDV01(yc)

except Exception as e:
    print  str(e) 

   
 # python hw3.py -y yc.csv -d mortgages.csv

# -*- coding: utf-8 -*-
"""
Created on Wed Sep 21 11:24:46 2016

@author: liu
"""
from decimal import *
import numpy as np
from collections import OrderedDict
import math
import scipy.optimize as optimize


class YieldCurve:
    """ use python interpolation package to obtain rates for each month based on input from Excel"""    
    def __init__(self, YC, rates=()):
       self.YC = YC #YC is data read from Excel
       ''' use numpy interpolation to calculate rates for all terms '''
       ordYC=OrderedDict()
       for key in sorted(self.YC):
           ordYC[key] = self.YC.get(key)
       TERM = 30*12    
       self.rates = np.interp(range(1  ,TERM+1), ordYC.keys(), ordYC.values() )

    def printYC(self):
        """return YC"""
        print self.YC

    def getRates(self):
        """return interpolated rates"""
        return self.rates

    def discountFactor(self,term):
        ''' semi-annual compounding  '''   
        return math.pow(1+self.rates[term-1]/200.,  -1.0*term/6.0)    

class MtgPkg:
    mtgs = []        
    # Add mortgage records
    def addMtg(self,principle, rt, term):
        mtg = Mtg(principle,rt,term)
        self.mtgs.append(mtg)

    # return all mortgages as a list
    def getMtgs(self):
        return self.mtgs

    #return total package value
    def getTtlValue(self,yc):
        ttl = 0.
        for mtg in self.getMtgs():
           ttl +=   mtg.mtgValue(yc)
        return ttl

    #return terms and coupon payments of the whole mortgage package
    def getCoupons(self):
        cp = 0. #payments
        tm = 0. #term
        for mtg in self.getMtgs():
           cp += mtg.mtgMonthlyPmt() 
           if mtg.term > tm:
               tm = mtg.term
        return tm, cp        
    #use scipy.optimize to find YTM
    def getYTM(self,yc): 
        tm, cp = self.getCoupons()
        bondValue = self.getTtlValue(yc)    
        ytm_func = lambda(y):sum([cp/(1+y/2.0)**(2.0*t/12.0) for t in range(1,tm+1)])-bondValue
        return optimize.newton(ytm_func,0.05)

    #McCauley duration
    def getMcCauleyDuration(self,yc):
        ttl = 0.
        bondValue = self.getTtlValue(yc)
        tm, cp = self.getCoupons()
        for num in range(1, tm + 1):
            ttl += cp * yc.discountFactor(num) * num/12.0
        
        return ttl/bondValue
    #convexity
    def getMcCauleyConvexity(self,yc):
        ttl = 0.
        bondValue = self.getTtlValue(yc)
        tm, cp = self.getCoupons()
        for num in range(1, tm + 1):
            ttl += cp * yc.discountFactor(num) * num/12.0 * num/12.0
        
        return ttl/bondValue
    #modified duration
    def getModifiedDuration(self,yc):
        md = self.getMcCauleyDuration(yc)
        ytm = self.getYTM(yc)
        return md/(1+ytm/2.0)     

    #modified convexity
    def getModifiedConvexity(self,yc):
        md = self.getMcCauleyDuration(yc)
        mc = self.getMcCauleyConvexity(yc)
        ytm = self.getYTM(yc)
        return (mc+md/2.)/(1+ytm/2.0)**2    
    #DV01
    def getDV01(self,yc):
        bps = 0.001
        tm, cp = self.getCoupons()
        ytm = self.getYTM(yc)
        val0 = self.getNewValue(cp, ytm, tm)
        val1 = self.getNewValue(cp, ytm+bps, tm)
        val2 = self.getNewValue(cp, ytm-bps, tm)
        return (abs(val1-val0)+abs(val2-val0))/(20.)
    
    def getNewValue(self, cp, y, tm):
        return sum([cp/(1+y/2.0)**(2.0*t/12.0) for t in range(1,tm+1)])
        
        
class Mtg:
  """defines one mortgage struct for each row read from Excel"""    
  principle = 0.
  rt = 0.
  term = 0.
  """constructor"""  
  def __init__(self,principle,rt,term):
      self.principle = principle
      self.rt = rt
      self.term = term
      
  """montly mortgage payment"""  
  def mtgMonthlyPmt(self):
      calculator = PmtCalculator()
      return calculator.pmt(self.principle,self.rt,self.term)
  
  """ttl value of this mortgage discounted with YC"""  
  def mtgValue(self, yc):
      ttl = 0.
      mpmt = self.mtgMonthlyPmt()
      for num in range(1, self.term + 1):
#        print yc.discountFactor(num)
        ttl += mpmt * yc.discountFactor(num)
        
      return ttl
      
class PmtCalculator:        
    """Calculate mortgage monthly payment"""
    def pmt(self,principal, rate, term):    
        ratePerTwelve = rate / (12.0)    
        result = principal * (ratePerTwelve / (1 - (1 + ratePerTwelve) ** (-term)))    
        return result
         
        
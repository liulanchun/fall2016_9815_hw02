# run the following command to execute the program
python fall2016_9815_hw02.py -y yc.csv -d mortgages.csv

# the main program is within a try block. Errors can be trapped properly.
# The total value, duration, convexity and DV01 of the mortgage package are calculated.
# scipy.optimize is used to seek YTM of the package
